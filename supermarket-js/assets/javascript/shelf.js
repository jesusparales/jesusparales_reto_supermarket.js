function Shelf(number, products) {
  this.number = number;
  this.products = products;
}

Shelf.prototype.render = function () {
  var $shelfContainer = document.createElement('div');
  $shelfContainer.classList.add('col-md-3', 'col-sm-6', 'shelf');
  $shelfContainer.innerHTML = "<h6 class ='aisle-name'>AISLE #"+this.number+"</h6>";
   for (let i = 0; i < this.products.length; i++) {
     $shelfContainer.appendChild( this.products[ Math.floor(Math.random() * this.products.length) ].build() );
   }
  return $shelfContainer;
}


