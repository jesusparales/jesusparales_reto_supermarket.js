function Product(name, price, url) {
  this.name = name;
  this.price = price;
  this.imageUrl = url;
}

Product.prototype.build = function () {

  var $p = document.createElement('div');
  $p.classList.add('product-card', 'card');

  var $img = document.createElement('img');
  $img.classList.add('card-img-top', 'product-card-img');
  $img.src = this.imageUrl;

  $p.innerHTML = this.name+" | $"+this.price;
  $p.prepend($img);

  return $p;
}




